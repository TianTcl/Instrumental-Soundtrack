@ECHO OFF
CLS
CALL :setESC

:checkpoint
git pull

IF %errorlevel% NEQ 0 GOTO failedSeq

ECHO %ESC%[92mSoundtracks updated successfully%ESC%[0m
TIMEOUT 14
EXIT /B

:failedSeq
ECHO %ESC%[91mFailed to get files.%ESC%[0m %ESC%[96mRestarting...%ESC%[0m
ECHO.
GOTO checkpoint

:setESC
REM FOR /F %a IN ('"ECHO PROMPT $E ^| CMD"') DO SET ESC=%a
FOR /F "tokens=1,2 delims=#" %%a IN ('"PROMPT #$H#$E# & ECHO on & FOR %%b IN (1) DO REM"') DO (
	SET ESC=%%b
	EXIT /B 0
)